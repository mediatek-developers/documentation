# Translation of Inkscape's tracing tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2018-05-01 12:58+0300\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>, 2018\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2018"

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
msgid "Tracing bitmaps"
msgstr "Векторизація растрових зображень"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Однією з функціональних можливостей «Inkscape» є інструмент векторизації "
"растрового зображення в елемент &lt;контуру&gt; для Вашого SVG-малюнку. Ці "
"короткі нотатки ознйомлюють з тим як це працює."

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Наразі «Inkscape» використовує рушій векторизації растрових зображень "
"«Potrace» (<ulink url=\"http://potrace.sourceforge.net\">potrace.sourceforge."
"net</ulink>) Пітера Селінджера (Peter Selinger). В майбутньому очікується "
"можливість використання допоміжних програм векторизації; наразі, однак, "
"цього дивовижного інструменту більш, ніж достатньо для наших потреб."

#. (itstool) path: article/para
#: tutorial-tracing.xml:23
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Майте на увазі, що метою «Трасувальника» не є відтворення точної копії "
"оригіналу зображення; також він не призначений для створення кінцевого "
"продукту. Жоден автотрасувальник не може зробити цього. Все, що він робить – "
"це надає набір кривих, які можна використовувати в якості джерела для своїх "
"малюнків."

#. (itstool) path: article/para
#: tutorial-tracing.xml:28
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"«Potrace» інтрепретує чорно-біле растрове зображення і створює набір кривих. "
"Для «Potrace», наразі, є три типи вхідних фільтрів для конвертації "
"необробленого зображення у щось, що «Potrace» може використовувати."

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Загалом, що більше темних пікселів в проміжному векторному зображенні, то "
"більше трасування виконає «Potrace». Якщо кількість трасувань зростає, то "
"буде необхідно більше процесорного часу і елемент &lt;контуру&gt; буде "
"значно більшим. Рекомендується, щоб спочатку користувач експериментував з "
"легшими проміжними зображеннями, поступово роблячи їх темнішими, щоб "
"отримати потрібну пропорцію і складність вихідного контуру."

#. (itstool) path: article/para
#: tutorial-tracing.xml:38
#, fuzzy
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Щоб застосувати трасувальник, заванажте або імпортуйте зображення, виділіть "
"його та виберіть <command>«Контур» &gt; «Векторизувати растр»</command> або "
"<keycap>Shift+Alt+B</keycap>."

#. (itstool) path: article/para
#: tutorial-tracing.xml:50
msgid "The user will see the three filter options available:"
msgstr "Користувачу буде доступно три опції фільтрування:"

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:55
msgid "Brightness Cutoff"
msgstr "Обмеження яскравості"

#. (itstool) path: article/para
#: tutorial-tracing.xml:60
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Просто використовується сума червоної, зеленою та синьої (або відтінків "
"сірого) складових пікселя в якості індикатору того, чи має він вважатися "
"чорним чи білим. Порогове значення може дорівнювати від 0,0 (чорний) до 1,0 "
"(білий). Що вище порогове значення, то менше пікселів буде вважатися "
"«білими» і проміжне зображення буде темнішим."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:75
msgid "Edge Detection"
msgstr "Визначення меж"

#. (itstool) path: article/para
#: tutorial-tracing.xml:80
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Використовується алгоритм визначення меж, розробленого Дж.Кенні (J. Canny), "
"як метод швидкого виявлення ізокліней з подібним контрастом. Цей алгоритм "
"створює проміжне растрове зображення, менш схоже на оригінал, ніж результат "
"«Порогу яскравості», але, ймовірно, надасть інформацію по кривих, яка інакше "
"проігнорується. Тут, значення порогу (0,0 – 1,0) підлаштовує поріг "
"яскравості, щоб включити в результат чи ні піксель, прилеглий до контрастної "
"межі. Цей параметр може коригувати темність або товщину межі результату."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:96
msgid "Color Quantization"
msgstr "Квантування кольорів"

#. (itstool) path: article/para
#: tutorial-tracing.xml:101
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Результат цього фільтру видає проміжне зображення, яке дуже відрізняється "
"від перших двох, але, без сумнівів, є дуже корисним. Замість відображення "
"ізоклін яскравості чи контрасту, буде знайдено межі, в яких змінюються "
"кольори навіть за однакової яскравості чи контрасту. Тут, параметр, "
"«Кількість кольорів» визначає, скільки вихідних кольорів було б якби "
"проміжне растрове зображення було кольоровим. Потім, вибирається між чорним "
"і білим в залежності від індексу парності кольору."

#. (itstool) path: article/para
#: tutorial-tracing.xml:115
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Користувач має спробувати всі ці три фільтри і поспостерігати за різними "
"типами результатів для різних типів вхідних зображень. Завжди будуть "
"зображення, для яких один фільтр підходить більше, аніж інші."

#. (itstool) path: article/para
#: tutorial-tracing.xml:119
#, fuzzy
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Після векторизації, вважається, що користувач використає <command>«Контур» "
"&gt; «Спростити»</command> (<keycap>Ctrl+L</keycap>)  для вихідного контуру "
"для зменшення кількості вузлів. Це може зробити результат «Potrace» значно "
"легшим для редагування. Наприклад, ось – типова векторизація «Старого "
"гітариста»:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:133
#, fuzzy
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Зверніть увагу на величезну кількість вузлів у контурі. Після натискання "
"<keycap>Ctrl+L</keycap>, ось – типовий результат:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:144
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Зображення трохи більше апроксимоване та грубе, але малюнок значно простіший "
"і легший для редагування. Майте на увазі, що потрібна не точна векторизація "
"зображення, а набір кривих, які можна використати для свого малюнку."

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Основні опції діалогового вікна «Векторизувати растр»"

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr "Початкове зображення"

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr "Поріг яскравості"

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Заповнення, без штриха"

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Штрих, без заповнення"

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr "Визначені межі"

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Квантування (12 кольорів)"

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Векторизоване зображення / Вихідний контур"

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1 551 nodes)"

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Векторизоване зображення / Вихідний контур – спрощений"

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 вузли)"
