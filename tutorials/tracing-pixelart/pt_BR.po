msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2020-01-11 15:29-0800\n"
"Last-Translator: Victor Westmann <victor.westmann@gmail.com>\n"
"Language-Team: \n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.4\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Victor Westmann <victor.westmann@gmail.com>, 2019"

#. (itstool) path: articleinfo/title
#: tutorial-tracing-pixelart.xml:6
msgid "Tracing Pixel Art"
msgstr "Vetorizar Arte de Píxeis"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing-pixelart.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:11
msgid "Before we had access to great vector graphics editing software..."
msgstr "Antes de termos acesso a programas de edição de gráficos vetoriais..."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:14
msgid "Even before we had 640x480 computer displays..."
msgstr "Mesmo antes de usarmos telas de computador com resolução de 640x480..."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:17
msgid ""
"It was common to play video games with carefully crafted pixels in low "
"resolutions displays."
msgstr ""
"Era comum jogar jogos de video game com píxeis criados especialmente para "
"telas de baixa resolução."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:20
msgid "We name \"Pixel Art\" the kind of art born in this age."
msgstr ""
"Este tipo de arte nascida nesta época nós chamamos de \"Arte em Píxeis\"."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:24
msgid ""
"Inkscape is powered by <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> with the ability to automatically vectorize these "
"\"special\" Pixel Art images. You can try other types of input images too, "
"but be warned: The result won't be equally good and it is a better idea to "
"use the other Inkscape tracer, potrace."
msgstr ""
"O Inkscape inclui o programa <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> que oferece a capacidade de vetorizar "
"automaticamente estas imagens \"especiais\" de Arte de Píxeis. Pode tentar "
"usar esta ferramenta noutros tipos de imagens, mas tenha em conta que o "
"resultado não será tão bom quanto o da ferramenta tradicional do Inkscape de "
"Vetorizar Imagens Bitmap."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:30
msgid ""
"Let's start with a sample image to show you the capabilities of this tracer "
"engine. Below there is an example of a raster image (taken from a Liberated "
"Pixel Cup entry) on the left and its vectorized output on the right."
msgstr ""
"Vamos começar com uma imagem de amostra para mostrar os recursos desse "
"mecanismo rastreador. Abaixo, há um exemplo de uma imagem rasterizada "
"(tirada de uma entrada Liberated Pixel Cup) à esquerda e sua saída "
"vetorizada à direita."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:41
msgid ""
"libdepixelize uses Kopf-Lischinski algorithm to vectorize images. This "
"algorithm uses ideas of several computer science techniques and math "
"concepts to produce a good result for pixel art images. One thing to notice "
"is that the alpha channel is completely ignored by the algorithm. "
"libdepixelize has currently no extensions to give a first-class citizen "
"treatment for this class of images, but all pixel art images with alpha "
"channel support are producing results similar to the main class of images "
"recognized by Kopf-Lischinski."
msgstr ""
"o motor libdepixelize usa o algoritmo Kopf-Lischinski para vetorizar "
"imagens. Este algoritmo usa ideias de várias técnicas da ciência da "
"computação e conceitos matemáticos para produzir um bom resultado em imagens "
"de Arte de Píxeis. Algo a notar é o canal alfa (transparência) que é "
"completamente ignorado pelo algoritmo. O libdepixelize não tem neste momento "
"extensões que suportem totalmente este tipo de imagens, mas todas as imagens "
"de Arte de Píxeis com suporte de canal alfa estão a produzir resultados "
"similares às imagens de referência reconhecidos por Kopf-Lischinski."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:55
msgid ""
"The image above has alpha channel and the result is just fine. Still, if you "
"find a pixel art image with a bad result and you believe that the reason is "
"the alpha channel, then contact libdepixelize maintainer (e.g. fill a bug on "
"the project page) and he will be happy to extend the algorithm. He can't "
"extend the algorithm if he don't know what images are giving bad results."
msgstr ""
"A imagem acima tem um canal alfa e o resultado está bom. Mesmo assim, se "
"encontrar uma imagem de Arte de Píxeis com um mau resultado e acredita que o "
"problema é o canal alfa, então contacte o responsável pelo libdepixelize "
"(isto é, submeta um bug na página do projeto) e nós ficaremos contentes por "
"integrar novas funcionalidades e correções. Nós não poderemos melhorar o "
"programa se não soubermos que tipos de imagens dão maus resultados."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:61
msgid ""
"The image below is a screenshot of <guimenuitem>Pixel art</guimenuitem> "
"dialog in the English localisation. You can open this dialog using the "
"<menuchoice><guimenu>Path</guimenu><guisubmenu>Trace Bitmap</"
"guisubmenu><guimenuitem>Pixel art</guimenuitem></menuchoice> menu or right-"
"clicking on an image object and then <guimenuitem>Trace Bitmap</guimenuitem>."
msgstr ""
"A imagem abaixo é uma captura de tela do diálogo de <guimenuitem>Arte em "
"Píxeis</guimenuitem> na localização em inglês. Você pode abrir este diálogo "
"usando o menu de arte <menuchoice> <guimenu>Caminho</"
"guimenu><guisubmenu>Vetorizar Bitmap</guisubmenu><guimenuitem>Arte em "
"Píxeis</guimenuitem></menuchoice> ou clicar direito em um objeto de imagem "
"e, em seguida, <guimenuitem>Vetorizar Bitmap</guimenuitem>."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:74
msgid ""
"This dialog has two sections: Heuristics and output. Heuristics is targeted "
"at advanced uses, but there already good defaults and you shouldn't worry "
"about that, so let's leave it for later and starting with the explanation "
"for output."
msgstr ""
"Este painel tem duas seções: Heurísticas e saída. As Heurísticas destinam-se "
"a utilizações avançadas, mas estas já têm uma configuração padrão para casos "
"gerais que produz bons resultados. Mas não nos preocupemos com esta parte "
"agora. Ficará para mais tarde. Começaremos pela explicação da secção Saída."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:79
msgid ""
"Kopf-Lischinski algorithm works (from a high-level point of view) like a "
"compiler, converting the data among several types of representation. At each "
"step the algorithm has the opportunity to explore the operations that this "
"representation offers. Some of these intermediate representations have a "
"correct visual representation (like the reshaped cell graph Voronoi output) "
"and some don't (like the similarity graph). During development of "
"libdepixelize users kept asking for adding the possibility of export these "
"intermediate stages to the libdepixelize and the original libdepixelize "
"author granted their wishes."
msgstr ""
"O algoritmo Kopf-Lischinski funciona (de um ponto de vista de alto-nível) "
"como um compilador, convertendo os dados ao longo de vários tipos de "
"representação. Em cada passo, o algoritmo tem a oportunidade de explorar "
"operações que esta representação fornece. Algumas destas representações "
"intermédias têm uma representação visual correta (como a saída Voronoi do "
"gráfico da célula reformulada) e algumas não têm (como o gráfico de "
"similaridade). Durante o desenvolvimento do libdepixelize, os utilizadores "
"pediram muitas vezes para adicionar a possibilidade de exportar estes passos "
"intermédios para o libdepixelize e o autor original aceitou este pedido."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:87
msgid ""
"The default output should give the smoothest result and is probably what you "
"want. You saw already the default output on the first samples of this "
"tutorial. If you want to try it yourself, just open the <guimenuitem>Trace "
"Bitmap</guimenuitem> dialog, select <guimenuitem>Pixel art</guimenuitem> tab "
"and click in <guibutton>OK</guibutton> after choosing some image on Inkscape."
msgstr ""
"A saída padrão deve fornecer o resultado mais suave e provavelmente é o que "
"você deseja. Você já viu a saída padrão nos primeiros exemplos deste "
"tutorial. Se você quiser tentar você mesmo, basta abrir a caixa de diálogo "
"<guimenuitem>Vetorizar Bitmap</guimenuitem>, selecione a "
"guia<guimenuitem>Arte em Píxeis</guimenuitem> e clique em <guibutton>OK</"
"guibutton> depois de escolher alguma imagem no Inkscape."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:93
msgid ""
"You can see the Voronoi output below and this is a \"reshaped pixel image\", "
"where the cells (previously pixels) got reshaped to connect pixels that are "
"part of the same feature. No curves will be created and the image continues "
"to be composed of straight lines. The difference can be observed when you "
"magnify the image. Previously pixels couldn't share a edge with a diagonal "
"neighbour, even if it was meant to be part of the same feature. But now "
"(thanks to a color similarity graph and the heuristics that you can tune to "
"achieve a better result), it's possible to make two diagonal cells share an "
"edge (previously only single vertices were shared by two diagonal cells)."
msgstr ""
"Pode-se ver a saída Voronoi abaixo que consiste numa \"imagem de píxeis com "
"a forma alterada\", onde as células (que anteriormente eram píxeis) foram "
"alteradas quanto à forma para ligar píxeis que fazem parte do mesmo recurso. "
"Nenhuma curva é criada e a imagem continua a ser composta por linhas retas. "
"A diferença pode ser observada quando se amplia a imagem. Os píxeis "
"anteriores não podiam partilhar uma borda com um vizinho na diagonal, mesmo "
"que tenham sido colocados para fazer parte do mesmo recurso. Mas agora "
"(graças à similaridade das cores e às heurísticas que podem ser ajustadas "
"para obter melhores resultados), é possível fazer com que duas células "
"diagonais partilhem uma borda (anteriormente apenas vértices únicos eram "
"partilhados por duas células diagonais)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:109
msgid ""
"The standard B-splines output will give you smooth results, because the "
"previous Voronoi output will be converted to quadratic Bézier curves. "
"However, the conversion won't be 1:1 because there are more heuristics "
"working to decide which curves will be merged into one when the algorithm "
"reaches a T-junction among the visible colors. A hint about the heuristics "
"of this stage: You can't tune them."
msgstr ""
"A saída padrão B-splines fornecerá resultados suaves, porque a saída "
"anterior do Voronoi será convertida em curvas quadráticas de Bézier. No "
"entanto, a conversão não será 1: 1 porque existem mais heurísticas "
"trabalhando para decidir quais curvas serão mescladas em uma quando o "
"algoritmo atingir uma junção T entre as cores visíveis. Uma dica sobre as "
"heurísticas deste estágio: você não pode ajustá-las."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:115
msgid ""
"The final stage of libdepixelize (currently not exportable by the Inkscape "
"GUI because of its experimental and incomplete state) is \"optimize curves\" "
"to remove the staircasing effect of the B-Spline curves. This stage also "
"performs a border detection technique to prevent some features from being "
"smoothed and a triangulation technique to fix the position of the nodes "
"after optimization. You should be able to individually disable each of these "
"features when this output leaves the \"experimental stage\" in libdepixelize "
"(hopefully soon)."
msgstr ""
"A etapa final do libdepixelize (atualmente não exportável pela interface do "
"Inkscape por ser experimental e incompleto) é \"otimizar curvas\" para "
"remover o efeito de escada das curvas B-spline. Esta etapa também usa uma "
"técnica de deteção de bordas para prevenir que algumas características sejam "
"suavizadas e uma técnica de triangulação para corrigir a posição dos nós "
"após a otimização. É possível desativar cada uma destas características "
"quando a saída deixar de estar no \"estado experimental\" no libdepixelize "
"(esperamos que seja brevemente)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:122
msgid ""
"The heuristics section in the gui allows you to tune the heuristics used by "
"libdepixelize to decide what to do when it encounters a 2x2 pixel block "
"where the two diagonals have similar colors. \"What connection should I keep?"
"\" is what libdepixelize asks. It tries to apply all heuristics to the "
"conflicting diagonals and keeps the connection of the winner. If a tie "
"happens, both connections are erased."
msgstr ""
"A secção das heurísticas na interface permite afinar as heurísticas usadas "
"pelo libdepixelize para decidir o que fazer quando encontra um bloco de "
"píxel 2x2 onde as duas diagonais têm cores similares. \"Que ligação devo "
"manter?\" é o que o libdepixelize pergunta. Este tenta aplicar todas as "
"heurísticas às diagonais em conflito e mantém a ligação ao vencedor. Se "
"ocorrer um empate, ambas as ligações são eliminadas."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:128
msgid ""
"If you want to analyze the effect of each heuristic and play with the "
"numbers, the best output is the Voronoi output. You can see more easily the "
"effects of the heuristics in the Voronoi output and when you are satisfied "
"with the settings you got, you can just change the output type to the one "
"you want."
msgstr ""
"Para analisar o efeito de cada heurística e experimentar vários valores, a "
"melhor saída é a Voronoi. Pode-se ver melhor os efeitos das heurísticas na "
"saída Voronoi e quando estiver bom, pode-se mudar o tipo de saída para o "
"pretendido."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:133
msgid ""
"The image below has an image and the B-Splines output with only one of the "
"heuristics turned on for each try. Pay attention to the purple circles that "
"highlight the differences that each heuristic performs."
msgstr ""
"A imagem abaixo tem uma imagem e a saída B-Spline com apenas uma das "
"heurísticas ativadas para cada tentativa. Preste atenção aos círculos "
"púrpuras que destacam as diferenças entre a performance de cada heurística."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:144
msgid ""
"For the first try (top image), we only enable the curves heuristic. This "
"heuristic tries to keep long curves connected together. You can notice that "
"its result is similar to the last image, where the sparse pixels heuristic "
"is applied. One difference is that its \"strength\" is more fair and it only "
"gives a high value to its vote when it's really important to keep these "
"connections. The \"fair\" definition/concept here is based on \"human "
"intuition\" given the pixel database analysed. Another difference is that "
"this heuristic can't decide what to do when the connections group large "
"blocks instead of long curves (think about a chess board)."
msgstr ""
"Para a primeira tentativa (imagem no topo), apenas foi ativada a heurística "
"curvas. Esta heurística tenta manter ligadas as curvas longas. Pode-se notar "
"que este resultado é semelhante à última imagem, onde é aplicada a "
"heurística de píxeis dispersos. Difere na sua \"força\" que é mais justa e "
"apenas atribui um valor alto ao voto quando é realmente importante manter "
"estas ligações. O conceito/definição de \"justo\" aqui é baseado na "
"\"intuição humana\" na base de dados de píxeis analisados. Outra diferença é "
"que esta heurística não consegue decidir o que fazer quando as ligações "
"agrupam grandes blocos em vez de curvas longas (tal como num tabuleiro de "
"xadrez)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:152
msgid ""
"For the second try (the middle image), we only enable the islands heuristic. "
"The only thing this heuristic does is trying to keep the connection that "
"otherwise would result in several isolated pixels (islands) with a constant "
"weight vote. This kind of situation is not as common as the kind of "
"situation handled by the other heuristics, but this heuristic is cool and "
"help to give still better results."
msgstr ""
"Para a segunda tentativa (a imagem do meio) apenas foi ativada a heurística "
"Ilhas. A única coisa que esta heurística faz é tentar manter a ligação que "
"de outra forma poderia resultar em vários píxeis isolados (ilhas) com um "
"peso de voto constante. Este tipo de situação não é comum como a situação "
"que acontece com as outras heurísticas, mas esta heurística é excelente e "
"ajuda a obter melhores resultados."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:158
msgid ""
"For the third try (the bottom image), we only enable the sparse pixels "
"heuristic. This heuristic tries to keep the curves with the foreground color "
"connected. To find out what is the foreground color the heuristic analyzes a "
"window with the pixels around the conflicting curves. For this heuristic, "
"you not only tune its \"strength\", but also the window of pixels it "
"analyzes. But keep in mind that when you increase the window of pixels "
"analyzed the maximum \"strength\" for its vote will increase too and you "
"might want to adjust the multiplier for its vote. The original libdepixelize "
"author think this heuristic is too greedy and likes to use the \"0.25\" "
"value for its multiplier."
msgstr ""
"Para a terceira tentativa (a imagem do fundo), apenas se ativou a heurística "
"Píxeis Dispersos. Esta heurística tenta manter as curvas ligadas à cor do "
"fundo. Para descobrir qual é a cor do fundo a heurística analisa uma janela "
"com os píxeis à volta das curvas em conflito. Nesta heurística pode-se "
"afinar a \"força\", assim como a janela de píxeis que analisa. Notar que "
"quando se aumenta a janela de píxeis analisados, a \"força\" máxima para o "
"seu voto aumentará também e poderá ser melhor ajustar o multiplicador para o "
"seu voto. O autor original do libdepixelize acha que esta heurística é "
"demasiada \"gananciosa\" e prefere usar o valor \"0.25\" no multiplicador."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:167
msgid ""
"Even if the results of the curves heuristic and the sparse pixels heuristic "
"give similar results, you may want to leave both enabled, because the curves "
"heuristic may give an extra safety that the important curves of contour "
"pixels won't be hampered and there are cases that can be only answered by "
"the sparse pixels heuristic."
msgstr ""
"Mesmo que o resultado da heurística curvas e píxeis dispersos dêem "
"resultados similares, você pode querer manter ambos habilitados, porque a "
"heurística curvas pode fornecer uma segurança extra para que as curvas "
"importantes dos píxeis de contorno não sejam descartadas e há casos que "
"apenas podem ser respondidos pela heurística píxeis dispersos."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:172
msgid ""
"Hint: You can disable all heuristics by setting its multiplier/weight values "
"to zero. You can make any heuristic act against its principles using "
"negative values for its multiplier/weight values. Why would you ever want to "
"replace behaviour that was created to give better quality by the opposite "
"behaviour? Because you can... because you might want a \"artistic\" "
"result... whatever... you just can."
msgstr ""
"Dica: Você pode desativar todas as heurísticas, definindo seus valores de "
"multiplicador / peso como zero. Você pode fazer qualquer ato heurístico "
"contra seus princípios usando valores negativos para seus valores "
"multiplicadores / peso. Por que você desejaria substituir o comportamento "
"criado para oferecer melhor qualidade pelo comportamento oposto? Porque você "
"pode ... porque você pode querer um resultado \"artístico\" ... tanto "
"faz ... você pode."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:178
msgid ""
"And that's it! For this initial release of libdepixelize these are all the "
"options you got. But if the research of the original libdepixelize author "
"and its creative mentor succeeds, you may receive extra options that broaden "
"even yet the range of images for which libdepixelize gives a good result. "
"Wish them luck."
msgstr ""
"E é isto! Para este lançamento inicial do libdepixelize, estas são todas as "
"opções disponíveis. Mas se a pesquisa do autor original do libdepixelize e o "
"seu mentor criativo for bem sucedida, poderemos ter no futuro outras opções "
"e outros tipos de imagens adequadas nas quais o libdepixelize pode dar bons "
"resultados. Deseje a eles boa sorte."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:183
msgid ""
"All images used here were taken from Liberated Pixel Cup to avoid copyright "
"problems. The links are:"
msgstr ""
"Todas as imagens usadas aqui são provenientes de Liberated Pixel Cup para "
"evitar problemas de direitos autorais. Os links são:"

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:188
msgid ""
"<ulink url=\"http://opengameart.org/content/memento\">http://opengameart.org/"
"content/memento</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:193
msgid ""
"<ulink url=\"http://opengameart.org/content/rpg-enemies-bathroom-tiles"
"\">http://opengameart.org/content/rpg-enemies-bathroom-tiles</ulink>"
msgstr ""

#. (itstool) path: Work/format
#: tracing-pixelart-f01.svg:49 tracing-pixelart-f02.svg:49
#: tracing-pixelart-f03.svg:49 tracing-pixelart-f04.svg:49
#: tracing-pixelart-f05.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#~ msgid "http://opengameart.org/content/memento"
#~ msgstr "http://opengameart.org/content/memento"

#~ msgid "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
#~ msgstr "http://opengameart.org/content/rpg-enemies-bathroom-tiles"
