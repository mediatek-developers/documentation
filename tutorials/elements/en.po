msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
msgid "Elements of design"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:127
#, no-wrap
msgid "Line"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:140
#, no-wrap
msgid "Shape"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:192
#, no-wrap
msgid "Size"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:153
#, no-wrap
msgid "Space"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:114
#, no-wrap
msgid "Color"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:166
#, no-wrap
msgid "Texture"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:179
#, no-wrap
msgid "Value"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:205
#, no-wrap
msgid "Balance"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:218
#, no-wrap
msgid "Contrast"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:231
#, no-wrap
msgid "Emphasis"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:244
#, no-wrap
msgid "Proportion"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:257
#, no-wrap
msgid "Pattern"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:271
#, no-wrap
msgid "Gradation"
msgstr ""

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:284
#, no-wrap
msgid "Composition"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid "This is a partial bibliography used to build this document."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:241
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:246
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2.htm"
"\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:251
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:256
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:261
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:266
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""

#. (itstool) path: Work/format
#: elements-f01.svg:48 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f01.svg:88
#, no-wrap
msgid "Elements"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f01.svg:101
#, no-wrap
msgid "Principles"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f01.svg:297
#, no-wrap
msgid "Overview"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr ""

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr ""
